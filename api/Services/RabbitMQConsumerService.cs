using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Data;
using Entities;

namespace Services
{
    public class RabbitMQConsumerService : BackgroundService
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;
        private readonly AppDbContext _context;
        private readonly AppConfiguration _appConfiguration;

        public RabbitMQConsumerService(AppDbContext context, AppConfiguration appConfiguration)
        {
            _context = context;
            _appConfiguration = appConfiguration;

            _connectionFactory = new ConnectionFactory() 
            { 
                HostName = _appConfiguration.RabbitMQHost, 
                Port = _appConfiguration.RabbitMQPort, 
                UserName = _appConfiguration.RabbitMQUser, 
                Password = _appConfiguration.RabbitMQPassword
            };

            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _queueName = _appConfiguration.RabbitMQQueue;

            _channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var value = Encoding.UTF8.GetString(body);

                // Odczytaj temat (topic)
                var topic = ea.RoutingKey;

                // Odczytaj dodatkowe nagłówki (id pojazdu, id pomiaru)
                var vehicleId = "";
                var measurementId = "";

                var headers = ea.BasicProperties.Headers;
                if (headers != null)
                {
                    if (headers.ContainsKey("mach"))
                    {
                        vehicleId = Encoding.UTF8.GetString((byte[])headers["mach"]);
                    }

                    if (headers.ContainsKey("meas"))
                    {
                        measurementId = Encoding.UTF8.GetString((byte[])headers["meas"]);
                    }
                }

                Console.WriteLine("ID pojazdu: {0}", vehicleId);
                Console.WriteLine("ID pomiaru: {0}", measurementId);
                Console.WriteLine("Wartość Pomiaru: {0}", value);

                var measurement = new Measurement
                {
                    MachineID = vehicleId,
                    Value = value,
                    MeasurmentID = measurementId
                };

                //_context.Measurements.Add(measurement);
                //_context.SaveChanges();
            };

            _channel.BasicConsume(queue: _queueName, autoAck: true, arguments: null, consumer: consumer);

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _channel.Close();
            _connection.Close();
        }
    }
}
