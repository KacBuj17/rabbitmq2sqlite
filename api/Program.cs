﻿using Data;
using Microsoft.EntityFrameworkCore;
using Services;
using Entities;

var builder = Host.CreateApplicationBuilder();
//var builder = Host.CreateEmptyApplicationBuilder(null);


builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix);

var appConfiguration = builder.Configuration.Get<AppConfiguration>();
ArgumentNullException.ThrowIfNull(appConfiguration.SqConnectionString);
builder.Services.AddSingleton(appConfiguration);

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlite(appConfiguration.SqConnectionString, x => x.UseNetTopologySuite()));

builder.Services.AddHostedService<RabbitMQConsumerService>();
builder.Logging.AddConsole();

var app = builder.Build();
await app.RunAsync();
