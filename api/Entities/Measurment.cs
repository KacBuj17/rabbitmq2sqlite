using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities
{
    public class Measurement
    {
        public int Id { get; set; }
        public string MeasurmentID { get; set; }
        public string MachineID { get; set; }
        public string Value { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
    }

}