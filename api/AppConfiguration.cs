﻿public sealed class AppConfiguration
{
    public static string AppConfigurationPrefix = "PanelEmotoAgh_";
    /// <summary>
    /// Be careful if you run the application from a different namespace on the kube service. Use: pg.default instead pg
    /// </summary>
    public string? SqConnectionString { get; set; }  = "Data Source= C:/Users/Kacper/Desktop/database/mydatabase.db";

    public string RabbitMQHost { get; set; } = "localhost";

    public int RabbitMQPort { get; set; } = 5672;

    public string RabbitMQUser { get; set; } = "admin";

    public string RabbitMQPassword { get; set; } = "admin";

    public string RabbitMQQueue { get; set; } = "test";

    public ushort RabbitMQPrefetchCount { get; set; } = 250;
}

